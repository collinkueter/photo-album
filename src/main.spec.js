const photoService = require("./main").PhotoService;
const cli = require("./main").cli;
const nock = require("nock");

describe("photo-album", () => {
  describe("PhotoService", () => {
    it("should query web service using argument passed", () => {
      nock("https://jsonplaceholder.typicode.com")
        .get("/photos")
        .query(actualQueryObject => {
          expect(actualQueryObject.albumId).toEqual("4");
          return true;
        })
        .reply(200, []);
      photoService.getPhotosByAlbumId(4);
    });
    it("should return a list of photos when successful response from web service", async () => {
      const res = [
        {
          albumId: 3,
          id: 101,
          title: "incidunt alias vel enim",
          url: "https://via.placeholder.com/600/e743b",
          thumbnailUrl: "https://via.placeholder.com/150/e743b"
        }
      ];
      nock("https://jsonplaceholder.typicode.com")
        .get("/photos")
        .query({ albumId: "3" })
        .reply(200, res);
      photoService.getPhotosByAlbumId(3).then(r => {
        expect(r).toEqual(res);
      });
    });
    it("should return empty list when no images found for album from web service", () => {
      nock("https://jsonplaceholder.typicode.com")
        .get("/photos")
        .query({ albumId: "101" })
        .reply(200, []);
      photoService.getPhotosByAlbumId(101).then(r => {
        expect(r).toEqual([]);
      });
    });
    it("should pass http status code in the error message to caller", () => {
      nock("https://jsonplaceholder.typicode.com")
        .get("/photos")
        .query({ albumId: "lkj234" })
        .reply(404, "Error fetching photos");
      photoService
        .getPhotosByAlbumId("lkj234")
        .then(r => {
          expect(true).toEqual(false);
        })
        .catch(e => {
          expect(e.message).toEqual("404");
        });
    });
  });
  describe("format output", () => {
    it("should return id and photo title in the following format: '[<photo-id>] <photo-title>'", () => {
      expect(
        cli.formatOutput({
          albumId: 3,
          id: 101,
          title: "incidunt alias vel enim",
          url: "https://via.placeholder.com/600/e743b",
          thumbnailUrl: "https://via.placeholder.com/150/e743b"
        })
      ).toEqual("[101] incidunt alias vel enim");
    });
  });
  describe("usage", () => {
    it("should return usage text", () => {
      expect(cli.usage()).toEqual(
        "photo-album requires an album number argument ex. 'photo-album 3'"
      );
    });
  });
  describe("album not exist message", () => {
    it("should return message with album id", () => {
      expect(cli.albumNotExistMessage(101)).toEqual(
        "album [101] does not exist, please try again"
      );
    });
  });
  describe("error message", () => {
    it("should return error message with status code passed", () => {
      expect(cli.errorMessage(101)).toEqual(
        "Unknown error occurred with status code: 101"
      );
    });
  });
  describe("cli.run", () => {
    it("should call usage when no arguments are passed", () => {
      spyOn(cli, "usage");
      cli.run(["/version/bin/node", "photo-album"]);
      expect(cli.usage).toHaveBeenCalled();
    });
    it("should call usage when more than one argument is passed", () => {
      spyOn(cli, "usage");
      cli.run(["/version/bin/node", "photo-album", "1", "2"]);
      expect(cli.usage).toHaveBeenCalled();
    });
    it("should call photo service with argument", async () => {
      spyOn(photoService, "getPhotosByAlbumId").and.returnValue(
        Promise.resolve([])
      );
      await cli.run(["/version/bin/node", "photo-album", "3"]);
      expect(photoService.getPhotosByAlbumId).toHaveBeenCalledWith("3");
    });
    it("should call formatOutput for each result", async () => {
      spyOn(cli, "formatOutput");
      spyOn(photoService, "getPhotosByAlbumId").and.returnValue(
        Promise.resolve([{}, {}, {}])
      );
      await cli.run(["/version/bin/node", "photo-album", "3"]);
      expect(cli.formatOutput).toHaveBeenCalledTimes(3);
    });
    it("should print message when album does not exist", async () => {
      spyOn(cli, "albumNotExistMessage");
      spyOn(photoService, "getPhotosByAlbumId").and.returnValue(
        Promise.resolve([])
      );
      await cli.run(["/version/bin/node", "photo-album", "101"]);
      expect(cli.albumNotExistMessage).toHaveBeenCalledWith("101");
    });
    it("should call error message when rejected promise from service", async () => {
      spyOn(cli, "errorMessage");
      spyOn(photoService, "getPhotosByAlbumId").and.returnValue(
        Promise.reject(new Error(404))
      );
      await cli.run(["/version/bin/node", "photo-album", "1abce01"]);
      expect(cli.errorMessage).toHaveBeenCalledWith("404");
    });
  });
});
