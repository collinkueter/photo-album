const cli = require("./main").cli;
const stdout = require("test-console").stdout;
const nock = require("nock");

describe("integration/platform specific tests", () => {
  it("should print usage when no arguments are passed", () => {
    var output = stdout.inspectSync(function() {
      cli.run(["/version/bin/node", "photo-album"]); // the first argv element is always the node executable
    });
    expect(output[0]).toEqual(
      "photo-album requires an album number argument ex. 'photo-album 3'\n"
    );
  });
  it("should print usage when more than one argument is passed", () => {
    var output = stdout.inspectSync(function() {
      cli.run(["/version/bin/node", "photo-album", "1", "2"]);
    });
    expect(output[0]).toEqual(
      "photo-album requires an album number argument ex. 'photo-album 3'\n"
    );
  });
  it("should print id and photo title in the following format: '[<photo-id>] <photo-title>'", async () => {
    const res = [
      {
        albumId: 3,
        id: 101,
        title: "incidunt alias vel enim",
        url: "https://via.placeholder.com/600/e743b",
        thumbnailUrl: "https://via.placeholder.com/150/e743b"
      }
    ];
    nock("https://jsonplaceholder.typicode.com")
      .get("/photos")
      .query({ albumId: "3" })
      .reply(200, res);
    var inspect = stdout.inspect();
    var a = await cli.run(["/version/bin/node", "photo-album", "3"]);
    inspect.restore();
    expect(inspect.output[0]).toEqual("[101] incidunt alias vel enim\n");
    return a;
  });
  it("should print all results and each on a new line", async () => {
    const res = [
      {
        albumId: 3,
        id: 101,
        title: "incidunt alias vel enim",
        url: "https://via.placeholder.com/600/e743b",
        thumbnailUrl: "https://via.placeholder.com/150/e743b"
      },
      {
        albumId: 3,
        id: 102,
        title:
          "eaque iste corporis tempora vero distinctio consequuntur nisi nesciunt",
        url: "https://via.placeholder.com/600/a393af",
        thumbnailUrl: "https://via.placeholder.com/150/a393af"
      },
      {
        albumId: 3,
        id: 103,
        title: "et eius nisi in ut reprehenderit labore eum",
        url: "https://via.placeholder.com/600/35cedf",
        thumbnailUrl: "https://via.placeholder.com/150/35cedf"
      }
    ];
    nock("https://jsonplaceholder.typicode.com")
      .get("/photos")
      .query({ albumId: "3" })
      .reply(200, res);
    var inspect = stdout.inspect();
    var a = await cli.run(["/version/bin/node", "photo-album", "3"]);
    inspect.restore();
    expect(inspect.output.length).toEqual(3);
    expect(inspect.output[0]).toEqual("[101] incidunt alias vel enim\n");
    expect(inspect.output[1]).toEqual(
      "[102] eaque iste corporis tempora vero distinctio consequuntur nisi nesciunt\n"
    );
    expect(inspect.output[2]).toEqual(
      "[103] et eius nisi in ut reprehenderit labore eum\n"
    );
    return a;
  });
  it("should print message when album does not exist", async () => {
    nock("https://jsonplaceholder.typicode.com")
      .get("/photos")
      .query({ albumId: "101" })
      .reply(200, []);
    var inspect = stdout.inspect();
    var a = await cli.run(["/version/bin/node", "photo-album", "101"]);
    inspect.restore();
    expect(inspect.output[0]).toEqual(
      "album [101] does not exist, please try again\n"
    );
    return a;
  });
  it("should print user friendly message when something is wrong with the service", async () => {
    nock("https://jsonplaceholder.typicode.com")
      .get("/photos")
      .query({ albumId: "lkj234" })
      .reply(404, "Error fetching photos");
    var inspect = stdout.inspect();
    var a = await cli.run(["/version/bin/node", "photo-album", "lkj234"]);
    inspect.restore();
    expect(inspect.output[0]).toEqual(
      "Unknown error occurred with status code: 404\n"
    );
    return a;
  });
});
