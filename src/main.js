const axios = require("axios");

const PhotoService = {
  getPhotosByAlbumId: async function(albumId) {
    return axios
      .get("https://jsonplaceholder.typicode.com/photos", {
        params: {
          albumId: albumId
        }
      })
      .then(r => {
        return r.data;
      })
      .catch(e => {
        throw new Error(e.response.status);
      });
  }
};

const cli = {
  formatOutput: function(photo) {
    return "[" + photo.id + "] " + photo.title;
  },
  usage: function() {
    return "photo-album requires an album number argument ex. 'photo-album 3'";
  },
  albumNotExistMessage: function(albumId) {
    return "album [" + albumId + "] does not exist, please try again";
  },
  errorMessage: function(statusCode) {
    return "Unknown error occurred with status code: " + statusCode;
  },
  run: async function(args) {
    if (args.length != 3) {
      return console.log(this.usage());
    }
    return PhotoService.getPhotosByAlbumId(args[2])
      .then(d => {
        if (d.length === 0) {
          console.log(this.albumNotExistMessage(args[2]));
        }
        for (var i = 0; i < d.length; i++) {
          console.log(this.formatOutput(d[i]));
        }
      })
      .catch(e => console.log(this.errorMessage(e.message)));
  }
};

module.exports = { cli, PhotoService };
