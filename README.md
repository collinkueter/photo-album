# Photo Album

This is a simple cli app that displays photo ids and titles in an album. The photos are pulled from this free web service (https://jsonplaceholder.typicode.com/photos).

# How To Build

1. [Download](https://nodejs.org/en/) and install NodeJS
2. Run `npm install` in the project root directory

# How To Test

1. [Build](#how-to-build) the project
2. Run `npm run test` in the project root directory

# How To Run

## First Option

1. [Build](#how-to-build) the project
2. Run `node photo-album` in the same directory as the `photo-album` file (the project root directory)

## Second Option

1. [Build](#how-to-build) the project
2. Run `chmod +x photo-album` in the project root directory (By default the file does not have execution permissions) 
3. Run `./photo-album` in the project root directory

## Third Option

1. [Build](#how-to-build) the project
2. Run `npm link` in the project root directory to globally install a symlink to `photo-album` 
3. Run by typing `photo-album` from any terminal from any location
  
## Fourth Option

Don't have or don't want to install node but already have Docker? Use these options to run the photo-album app using Docker. Don't have Docker? [Download](https://www.docker.com/products/docker-desktop) and install Docker Desktop to try the following options. 

### Build And Run Locally

1. Run `docker build -t <imagename> .` from the project root directory to build the image
2. Run `docker run --rm <imagename> ./photo-album <albumId>` from anywhere to run the app

Here is an example: 
1. `docker build -t photo-album .`
2. `docker run --rm photo-album ./photo-album 3`

### Run Online

Want to take it a step further and run without installing anything!?

1. Navigate to [Play with Docker](https://labs.play-with-docker.com)
2. Click Start
3. Click Add New Instance
4. Run `docker run --rm collinkueter/photo-album ./photo-album 3` in the terminal