FROM mhart/alpine-node:12
WORKDIR /app
COPY package.json package-lock.json ./
COPY ./src ./src
COPY photo-album .
RUN npm ci --prod

# Only copy over the node pieces we need from the above image
FROM mhart/alpine-node:slim-12
WORKDIR /app
COPY --from=0 /app .
COPY . .